#!/bin/sh
#
# verify that varnish is running before vi try installing the vmod
#

test_description="Check for running varnish"

. "/usr/share/sharness/sharness.sh"

test_expect_success "varnish package installed" '
  dpkg -l varnish
'

test_expect_success "varnishadm ping" '
  varnishadm ping
'

test_done
