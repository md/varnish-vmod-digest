#!/bin/sh
#
# verify that varnish is running before vi try installing the vmod
#

test_description="Load VCL with vmod"

. "/usr/share/sharness/sharness.sh"

test_expect_success "Load VCL with vmod" '
  varnishadm vcl.load test /etc/varnish/test.vcl
'

test_expect_success "Use VCL with vmod" '
  varnishadm vcl.use test
'

test_done
